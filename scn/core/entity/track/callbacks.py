import mathutils

from scn.core.entity.track import validate
from scn.core.geometry.track import create_track_geometry_manager


def update_geometry(self, context):
    obj = self.id_data
    mngr = create_track_geometry_manager(obj)

    container = mngr.get_container_from(obj)
    old_materials = []
    if container:
        old_materials = [slot.material for slot in container.material_slots]

    geometry = mngr.create(obj)
    mngr.set(obj, geometry)

    for i, old_material in enumerate(old_materials):
        update_materialX(obj, old_material, i)


def get_length(self):
    obj = self.id_data
    if validate.is_curve(obj):
        return obj.data.splines[0].calc_length()

    return 0

def update_visibility(self, context):
    obj = self.id_data
    mngr = create_track_geometry_manager(obj)

    profile_container = mngr.get_container_from(obj)
    if profile_container:
        profile_container.hide_viewport = obj.eu07_track.visibility == "unvis"
    
    
def update_material1(self, context):
    obj = self.id_data

    mngr = create_track_geometry_manager(obj)
    geometry = mngr.create(obj)
    mngr.set(obj, geometry)

    update_materialX(obj, obj.eu07_track.material1, 0)
    update_materialX(obj, obj.eu07_track.material2, 1)


def update_material2(self, context):
    obj = self.id_data
    material = obj.eu07_track.material2
    update_materialX(obj, material, 1)


def update_materialX(obj, material, slot_index):
    mngr = create_track_geometry_manager(obj)

    profile_container = mngr.get_container_from(obj)
    if profile_container:
        slots = profile_container.material_slots
        if not slots:
            for _ in range(2):
                profile_container.data.materials.append(None)

        slot = profile_container.material_slots[slot_index]
        slot.link = 'OBJECT'
        slot.material = material


def get_p1(self):
    obj = self.id_data
    return get_pX(obj, 1)


def set_p1(self, value):
    obj = self.id_data
    set_pX(obj, 1, value)


def get_p2(self):
    obj = self.id_data
    return get_pX(obj, 2)


def set_p2(self, value):
    obj = self.id_data
    set_pX(obj, 2, value)


def get_p3(self):
    obj = self.id_data
    return get_pX(obj, 3)


def set_p3(self, value):
    obj = self.id_data
    set_pX(obj, 3, value)


def get_p4(self):
    obj = self.id_data
    return get_pX(obj, 4)


def set_p4(self, value):
    obj = self.id_data
    set_pX(obj, 4, value)


def get_cv1(self):
    obj = self.id_data
    return get_cvX(obj, 1)


def set_cv1(self, value):
    obj = self.id_data
    return set_cvX(obj, 1, value)


def get_cv2(self):
    obj = self.id_data
    return get_cvX(obj, 2)


def set_cv2(self, value):
    obj = self.id_data
    return set_cvX(obj, 2, value)


def get_cv3(self):
    obj = self.id_data
    return get_cvX(obj, 3)


def set_cv3(self, value):
    obj = self.id_data
    return set_cvX(obj, 3, value)


def get_cv4(self):
    obj = self.id_data
    return get_cvX(obj, 4)


def set_cv4(self, value):
    obj = self.id_data
    return set_cvX(obj, 4, value)


def get_pX(obj, x):
    spline_idx, point_idx = get_spline_and_point_indices(x)

    if validate.is_curve(obj):
        try:
            return obj.matrix_world @ obj.data.splines[spline_idx].bezier_points[point_idx].co
        except:
            pass

    return mathutils.Vector((0, 0, 0))


def set_pX(obj, x, value):
    spline_idx, point_idx = get_spline_and_point_indices(x)


    if validate.is_curve(obj):
        try:
            point = obj.data.splines[spline_idx].bezier_points[point_idx]
            point.co = value

            point.handle_left_type = 'FREE'
            point.handle_right_type = 'FREE'
        except IndexError:
            pass


def get_spline_and_point_indices(x):
    spline_idx = 0
    point_idx = x - 1

    if x > 2:
        spline_idx = 1
        point_idx = 4 - x

    return spline_idx, point_idx


def get_cvX(obj, x):
    spline_idx, point_idx = get_spline_and_point_indices(x)

    if validate.is_curve(obj):
        try:
            point = obj.data.splines[spline_idx].bezier_points[point_idx]
        except IndexError:
            return mathutils.Vector((0, 0, 0))

        if (x + spline_idx) % 2 == 0:
            # adding spline_idx seems to fix the handle flipping issue
            # probably no need for more sophisticated fix as we only handle at most two pieces of spline
            return obj.matrix_world @ point.handle_left
        else:
            return obj.matrix_world @ point.handle_right


def set_cvX(obj, x, value):
    spline_idx, point_idx = get_spline_and_point_indices(x)

    if validate.is_curve(obj):
        point = obj.data.splines[spline_idx].bezier_points[point_idx]
        # TODO: Relative to pX
        if (x + spline_idx) % 2 == 0: 
            point.handle_left = value
            point.handle_right = 0, 0, 0
            point.handle_left_type = 'FREE'
        else:
            point.handle_right = value
            point.handle_left = 0, 0, 0
            point.handle_right_type = 'FREE'


def get_cvX_local(obj, x):
    cvX = get_cvX(obj, x)
    pX = get_pX(obj, x)
    return cvX - pX


def set_cvX_local(obj, x, value):
    pX = get_pX(obj, x)
    cvX = pX + mathutils.Vector(value)
    set_cvX(obj, x, cvX)